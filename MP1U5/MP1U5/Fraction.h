#pragma once
#include<iostream>
#include<exception>
class Fraction
{
public:
	Fraction(std::istream&in) try 
	{
        int num, denum;
        if (in >> num >> denum)
        {
            if (denum == 0)
            {
                throw std::runtime_error("denumenator is equial to 0");
            }
            numerator_=num;
            denominator_=denum;

        }
        else
        {
            throw std::exception("invalid data");
        }
	}
    catch (...) 
    {
        throw;
    }
	friend std::istream& operator>>(std::istream&, Fraction&);
	
private:
	int numerator_, denominator_;
};

